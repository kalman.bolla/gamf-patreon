Mobil alkalmazások fejlesztése szakirány contributors (érkezési sorrendben)
============================================

* **[Véninger Attila](https://hu.linkedin.com/in/attilaveninger)**

  * Első hivatalos támogatói cím

* **[Komlósi Zsolt](https://hu.linkedin.com/in/zsolt-komlosi)**

  * Második hivatalos támogatói cím
  * 
  
* **[Raffai Zoltán](https://www.linkedin.com/in/zolt%C3%A1n-raffai-a891687b/)**

  * Harmadik hivatalos támogatói cím
